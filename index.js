let num = 2;
let getCube = num ** 3;

let getCubeAnswer = `
The cube of ${num} is ${getCube}.`
console.log(getCubeAnswer);


/*  */
const fullAddress = ["258 Washington Ave", "NW, California", "USA"];

const [stAdd, cityState, country] = fullAddress;


console.log(`I live at ${stAdd} ${cityState}, ${country}`);
/*  */

let animal = {
    name: "Lolong",
    kind: "saltwater crocodile",
    weight: 1075,
    ft: 20,
    inch: 3

};

const { name, kind, weight, ft, inch } = animal;

function getAnimalDetails({ name, kind, weight, ft, inch }) {
    console.log(`${name} was a ${kind}. He weighed at ${weight} kgs with a measurement of ${ft} ft ${inch} in.`)
};

getAnimalDetails(animal);
/*  */
let numbers = [1, 2, 3, 4, 5]

numbers.forEach(number => {
    console.log(`${number}`);

});


/*  */



class Dog {
    constructor(breed, name, age) {
        this.breed = breed;
        this.name = name;
        this.age = age;
    };
};

const myDog = new Dog();

myDog.breed = "Miniature Dachshund";
myDog.name = "Frankie";
myDog.age = 5;

console.log(myDog);
